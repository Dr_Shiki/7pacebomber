﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Bomber
{
   class BombField
    {
        private int[,] btmField;
        private int height, width, bombsCount;
        public int Height
        {
            set
            {
                if (value <= 0)
                    throw new Exception("Negative height or equal zero");
                else
                    height = value;
            }
            get
            {
                if (height != null)
                    return height;
                else
                    throw new Exception("Height is undefined!");
            }
        }
        public int Width
        {
            set
            {
                if (value <= 0)
                    throw new Exception("Negative width or equal zero");
                width = value;
            }
            get
            {
                if (width != null)
                    return width;
                else
                    throw new Exception("Width is undefined");
            }
        }
        public int BombsCount
        {
            set
            {
                if (value < 0)
                    throw new Exception("Count of the bombs is negative");
                bombsCount = value;
            }
            get
            {
                if (bombsCount != null)
                    return bombsCount;
                throw new Exception("Count of the bombs is not defined");
            }
        }
        private void makeMap()
        {
            if (width * height < bombsCount)
                throw new Exception("Too many bombs for such map ");
            btmField = new int[width,height];
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    btmField[i,j] = 0;

            Random rand = new Random();

            List<Point> bomblocation = new List<Point>();
            while (bomblocation.Select(x=>x).Distinct().Count()< bombsCount)
            {
                bomblocation.Add(new Point(rand.Next(0, width), rand.Next(0, height)));
            }
            bomblocation = bomblocation.Select(x => x).Distinct().ToList();

            foreach (var i in bomblocation)
            {
                btmField[i.X,i.Y] = 10;
                for (int k = 0; k < 3; k++)
                    for (int p = 0; p < 3; p++)
                    {
                        if ((i.X - 1 + k < width && i.Y - 1 + p < height) && (i.X - 1 + k >= 0 && i.Y - 1 + p >= 0))
                            if (btmField[i.X - 1 + k, i.Y - 1 + p] != 10)
                                btmField[i.X - 1 + k, i.Y - 1 + p]++;
                    }
            }
        }
        public int[,] getArr()
        {
            if (btmField != null)
                return btmField;
            else
                throw new Exception("Base map massive is undefined!");
        }
        public string[] mapGenerator(int Width,int Height, int BombsCount)
        {
            try
            {
                width = Width;
                height = Height;
                bombsCount = BombsCount;
            }
            catch(Exception e)
            {
                throw e;
            }
            string[] map = new string[Width * Height];
            try
            {
                makeMap();
            }
            catch(Exception e)
            {
                throw e;
            }
            int strItter = 0;
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    switch(btmField[i,j])
                    {
                        case 0:
                            map[strItter] = "_";
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            map[strItter] = btmField[i,j].ToString();
                            break;
                        case 10:
                            map[strItter] = "*";
                            break;
                        default:
                            map[strItter] = "?";
                            break;
                    }
                    strItter++;
                }
            return map;
        }
        public string[] mapGenerator()
        {
            string[] map = new string[Width * Height];
            try
            {
                makeMap();
            }
            catch (Exception e)
            {
                throw e;
            }
            int strItter = 0;
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    switch (btmField[i, j])
                    {
                        case 0:
                            map[strItter] = "_";
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            map[strItter] = btmField[i, j].ToString();
                            break;
                        case 10:
                            map[strItter] = "*";
                            break;
                        default:
                            map[strItter] = "?";
                            break;
                    }
                    strItter++;
                }
            return map;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bomber
{
    public partial class Form1 : Form
    {
        List<Point> visitedPoints = new List<Point>();
        int height = 0;
        int width = 0;
        int bombCount = 0;
        double xScale = 1;
        double yScale = 1;
        Bitmap outMap;
        int[,] innerMap;
        int lineWidth = 4;
        Pen linePen;
        BombField fildMaker = new BombField();
        private void evalScale()
        {
            xScale = (pictureBox1.Width- lineWidth) / width;
            yScale =( pictureBox1.Height- lineWidth) / height;
            if (xScale < 1 || yScale < 1)
                throw new Exception("impossible to create such map, cause a cell of map smoller then one pixel");
            pictureBox1.Width = pictureBox1.Width % (int)Math.Ceiling(xScale) + lineWidth;
            pictureBox1.Height = pictureBox1.Height % (int)Math.Ceiling(yScale) + lineWidth;
        }
        public Form1()
        {
            InitializeComponent();
        }
        private Graphics DrawField()
        {
            linePen = new Pen(new SolidBrush(Color.Green), lineWidth);
            outMap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics e = Graphics.FromImage(outMap);
            for (int i = lineWidth / 2; i < outMap.Width; i += (int)Math.Ceiling(xScale))
                e.DrawLine(linePen, new Point(i, lineWidth / 2), new Point(i, outMap.Height - lineWidth / 2));
            for (int i = lineWidth / 2; i < outMap.Height; i += (int)Math.Ceiling(yScale))
                e.DrawLine(linePen, new Point(lineWidth / 2, i), new Point(outMap.Width- lineWidth / 2, i));
            return e;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!(int.TryParse(textBox1.Text, out height) && int.TryParse(textBox2.Text, out width) && int.TryParse(textBox3.Text, out bombCount)))
            {
                MessageBox.Show("Incorrect input value. Check textboxes");
                return;
            }
            try
            {
                evalScale();
                fildMaker.Height = height;
                fildMaker.Width = width;
                fildMaker.BombsCount = bombCount;
                Graphics gr = DrawField();
                makeLog(fildMaker.mapGenerator());
                innerMap = fildMaker.getArr();
                drawMapFill(gr);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            pictureBox1.Image= outMap;
        }
        private Color defineColor(int value)
        {
            switch(value)
            {
                case (0):
                    return Color.Gray;
                case (1):
                    return Color.Blue;
                case (2):
                    return Color.Aqua;
                case (3):
                    return Color.Green;
                case (4):
                    return Color.Yellow;
                case (5):
                    return Color.Orange;
                case (6):
                    return Color.Red;
                case (7):
                    return Color.Firebrick;
                case (8):
                    return Color.DarkRed;
                case (10):
                    return Color.Black;
                default: return Color.White;
            }
        }
        private void drawMapFill(Graphics e)
        {
            for (int i = lineWidth / 2, innerI=0; i < (outMap.Width)&&innerI<width; i += (int)Math.Ceiling(xScale),innerI++)
                for (int j = lineWidth / 2, innerJ=0; (j < outMap.Height)&&innerJ<height; j += (int)Math.Ceiling(yScale),innerJ++)
                    e.DrawString(innerMap[innerI, innerJ].ToString(), new Font(new FontFamily("Times New Roman"), (float)(yScale/2)), new SolidBrush(defineColor(innerMap[innerI, innerJ])), new PointF(i, j));
        }
        private void makeLog(string[] text)
        {
            int itter = 0;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    richTextBox1.Text += text[itter] + ";";
                    itter++;
                }
                richTextBox1.Text += "\n\r";
            }
        }
    }
}
